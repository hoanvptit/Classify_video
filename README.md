!cp "/content/drive/My Drive/dataset.rar" .
!mkdir "./dataset"
!unrar x dataset.rar "./dataset"
import cv2     
import math   
import numpy as np   
import os 
from tensorflow.keras import utils as np_utils
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
import random
from imutils import paths
def maxa(x):
    max = 0;
    j = -1;
    for i, y in  enumerate(x) :
      if y > max : 
        max = y
        j = i
    return j
def my_confusion_matrix(y_true, y_pred):
    N = y_true.shape[1] 
    cm = np.zeros((N, N))
    for n in range(y_true.shape[0]):
        x = maxa(y_true[n])
        y = maxa(y_pred[n])
        cm[ x , y ] += 1
       
    return cm 

X_test = []
Y_test = []
Z_test = []
sports = ["Soccer","TableTennis","Tennis","Volleyball","Bia"]
for i, sport in enumerate(sports) :
    list_paths = list(paths.list_images("/content/dataset/{0}_test".format(sport)))
    for path in list_paths:
        X_test.append(cv2.imread(path))
        Y_test.append(i)
        Z_test.append(path)

Y_test =  np_utils.to_categorical(Y_test, 5)
X_test =  np.array(X_test)
Z_test =  np.array(Z_test)
print(X_test.shape)
print(Y_test.shape)
print(Z_test.shape)


X_val = []
Y_val = []
Z_val = []
sports = ["Soccer","TableTennis","Tennis","Volleyball","Bia"]
for i, sport in enumerate(sports) :
    list_paths = list(paths.list_images("/content/dataset/{0}_val".format(sport)))
    for path in list_paths:
        X_val.append(cv2.imread(path))
        Y_val.append(i)
        Z_val.append(path)

Y_val =  np_utils.to_categorical(Y_val, 5)
X_val =  np.array(X_val)
Z_val =  np.array(Z_val)
print(X_val.shape)
print(Y_val.shape)
print(Z_val.shape)


X_train = []
Y_train = []
Z_train = []
sports = ["Soccer","TableTennis","Tennis","Volleyball","Bia"]
for i, sport in enumerate(sports) :
    list_paths = list(paths.list_images("/content/dataset/{0}_train".format(sport)))
    for path in list_paths:
        X_train.append(cv2.imread(path))
        Y_train.append(i)
        Z_train.append(path)

Y_train = np_utils.to_categorical(Y_train, 5)
X_train =  np.array(X_train)
Z_train =  np.array(Z_train)
print(X_train.shape)
print(Y_train.shape)
print(Z_train.shape)

from tensorflow.keras.applications import MobileNet
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input
mobileNet = MobileNet(weights='imagenet', include_top=False ,input_shape = (224,224,3))

fcHead = mobileNet.output

fcHead = Flatten()(fcHead)

fcHead = Dense(256, activation='relu')(fcHead)

fcHead = Dense(5, activation='softmax')(fcHead)

model = Model(inputs=mobileNet.input, outputs=fcHead)

model.summary()
model.compile(loss='categorical_crossentropy',
optimizer='adam',
metrics=["accuracy"])   

model.fit(X_train, Y_train, validation_data=(X_val, Y_val),

batch_size=50, epochs=10, verbose=1)

soccer = model.evaluate(X_test,Y_test)
Y_pred = model.predict(X_test)
print(my_confusion_matrix(Y_test,Y_pred))
print(soccer)

import tensorflow as tf
from keras.models import load_model
model = load_model('/content/tuan3.h5')
saved_model_dir = '/content/tuan'
tf.saved_model.save(model, saved_model_dir)
converter = tf.lite.TFLiteConverter.from_saved_model(saved_model_dir)
tflite_model = converter.convert()
with open('model.tflite', 'wb') as f:
    f.write(tflite_model)